'use strict';
/*
  Controlador de la vistsa student.js
 */
angular.module('tesisApp')
  .controller('StudentCtrl', function ($scope,$cookieStore,$location) {
    /*
      Controlador que coloca las imagenes en el menu de student.js
     */
    console.log('Welcome To UruMovil');
  	
    $scope.options = [{
  		'link' :'#/courses',
  		'img':'images/courses.png'
  	},{
  		'link':'#/status',
  		'img':'images/status2.png'
  	},{
  		'link':"#/schedule",
  		'img':'images/schedule.png'
  	}];
    $scope.closeSession = function(){
      console.log('Closing....');
      localStorage.clear();
    }
  });
