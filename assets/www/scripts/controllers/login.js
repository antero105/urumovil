'use strict';
/*
  Controlador de la vista Login
 */
angular.module('tesisApp')
  .controller('LoginCtrl', function ($scope,$location,$cookieStore,$http,login) {
   
    /*
      Metodo que envia la solicitud de ingreso, guardando la cedula en localStorage que sera
      usada como llave para mantener sesion. Se mantiene la sesion con localStorage
     */
    $scope.login = function(id,pass){
        if(typeof id =='undefined' || typeof pass =='undefined'){
            $('#alert').show("slow");
             setTimeout(function() {
                $('#alert').hide('slow');
            }, 3000);
        }
        else{
          
          angular.element('#mydiv').show();
          login.save({id:id,pass:window.btoa(pass)}).$promise.then(function(data){
            
            angular.element('#mydiv').hide();
            
            console.log('Login....');
            
            window.localStorage.setItem("id",id);
            
            $location.path('/student');
          
          }).catch(function(err){
            
            console.log(err);
            angular.element('#mydiv').hide();
            
            $('#alertErr').show("slow");
             setTimeout(function() {
                $('#alertErr').hide('slow');
            }, 3000);
          });
           
          
          
        }
        
    
	

    }
  });
