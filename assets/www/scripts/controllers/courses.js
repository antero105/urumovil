'use strict';
/*
    Controllador de la vista courses.html
 */
angular.module('tesisApp')
  .controller('CoursesCtrl', function ($scope,$cookieStore,courses) {
        /*
            Se hace una solicitud al servicio courses, y se pasa los resultados a la vista
            a travez de la variable courses.
         */
        
        angular.element('#mydiv').show();
        
        /*
            Solicitud al servicio, se mantiene sesion con localStorage
         */
        courses.get({
            id:window.localStorage.getItem("id")
        }).$promise.then(function(courses){
            console.log('Courses are here');
            
            angular.element('#mydiv').hide();
            
            $scope.courses = courses.Courses;
        }).catch(function(err){
            
            console.log(err);
            
            angular.element('#mydiv').hide();
            $('#alertDanger').show("slow");
             setTimeout(function() {
                $('#alertErr').hide('slow');
            }, 3000);
            
        });
    	
        /*
            Metodo que recibe el valor de la calificación, si la calificacion:
                *Es DIF,I,SI la calificación es amarilla.
                *Es mayor o igual a 10 o AP es verde
                *Si es menor que dia o APL o API es roja.
         */
        $scope.qualificationClass = function(qualification){
    		
    		if(qualification.trim() == 'DIF' || qualification.trim() == 'I'|| qualification.trim() == 'SI'){
    			
    			return 'label label-warning';
    		}
    		else if(parseInt(qualification) >=10 || qualification.trim() == 'AP'){
    			
    			return 'label label-success';

    		}
    		else if(parseInt(qualification)<10 || qualification.trim() == 'APL'|| qualification.trim() == 'API'){
    			
    			return 'label label-danger';
    		}
    	}
  });
