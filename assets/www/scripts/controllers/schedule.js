'use strict';
/*
    Controlador de la vista schedule.html
 */
var index;
angular.module('tesisApp')
  .controller('ScheduleCtrl', function ($scope,$http,$cookieStore,schedule){
    /*
        Con animos de guardar la misma sensacion de usuario se decidio mantener el mismo algoritmo
        usado anteriormente en la URU, para dibujar los horarios.
        Todo esta en la vista. Se mantgiene la sesion con localStorage
     */
    $scope.range = function(n) {
        return new Array(n);
    };
    $scope.setIndex= function(index2){
        index = index2 +1;
    }; 
    $scope.getIndex = function(){
        return index;
    };

    angular.element('#mydiv').show();
    schedule.get({
        id:window.localStorage.getItem("id")}).$promise.then(function(data){
            console.log('Schedule is here!!!');
            angular.element('#mydiv').hide();
            console.log(data);
            $scope.courses = data.Schedule;
    }).catch(function(data){
            console.log(data);
            angular.element('#mydiv').hide();
            $('#alertDanger').show("slow");
             setTimeout(function() {
                $('#alertErr').hide('slow');
            }, 3000);
    });
  });
